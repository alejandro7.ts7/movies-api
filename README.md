## Name
Movies API.

## Description
This is an API to get and create movies data and make a rating of them. Also you can create your own watchlist


## Installation

### With Docker (on terminal)

Go to folder of movies_api
**cd movies_api**

Run docker compose
**docker-compose up --build**

### Without docker
- Run Postgresql
- Create a new user with credentials on **.envs** folder
- Create database called 'movies'
- Run migrations `python manage.py migrate`
- Run fixtures:
    - `python manage.py loaddata fixtures/watchlists_fixtures.json --app users.user`
    - `python manage.py loaddata fixtures/watchlists_fixtures.json --app movies.genre`
    - `python manage.py loaddata fixtures/watchlists_fixtures.json --app movies.movie`
    - `python manage.py loaddata fixtures/watchlists_fixtures.json --app rating.ratings`
    - `python manage.py loaddata fixtures/watchlists_fixtures.json --app watchlists.watchlist`
- Run tests `python manage.py tests`
- Run the API `python manage.py runserver`
- Go to admin default page with default super user:
    - username: admin
    - password: admin



## Endpoints:

- Main url 'http://127.0.0.1:8000/';

### Auth endpoints
- SIGININ (POST) `http://127.0.0.1:8000/api/auth/signin/`
- SIGNUP (POST) `http://127.0.0.1:8000/api/auth/signup/`

### Movies
- CREATE MOVIE (POST) `http://127.0.0.1:8000/api/movies/create`
- MOVIES (GET) `http://127.0.0.1:8000/api/movies/`
- MOVIE_BY_ID (GET) `http://127.0.0.1:8000/api/movies/id`
- DELETE_MOVIE (DELETE) `http://127.0.0.1:8000/api/movies/id/delete`
- UPDATE_MOVIE (UPDATE) `http://127.0.0.1:8000/api/movies/id/update`


### Ratings
- CREATE_RATING (POST) `http://127.0.0.1:8000/api/ratings/create`
- RATINGS (GET) `http://127.0.0.1:8000/api/ratings/`
- RATING_BY_ID (GET) `http://127.0.0.1:8000/api/ratings/id`
- DELETE_RATING (DELETE) `http://127.0.0.1:8000/api/ratings/id/delete`
- UPDATE_RATING (UPDATE) `http://127.0.0.1:8000/api/ratings/id/update`


### Watchlist
- ADD_MOVIE (POST) `http://127.0.0.1:8000/api/watchlists/`
- WATCHLIST (GET) `http://127.0.0.1:8000/api/watchlists/add`
- REMOVE_MOVIE (DELETE) `http://127.0.0.1:8000/api/watchlists/id/remove`
