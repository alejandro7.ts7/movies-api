from django.apps import AppConfig


class RaitingsConfig(AppConfig):
    name = "core.ratings"
