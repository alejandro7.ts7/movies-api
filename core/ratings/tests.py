from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from core.movies.models import Genre, Movie
from core.users.models import User
from core.utils.http_status import HTTP_STATUS

from .models import Rating

# Create your tests here.


class RatingModelViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.genre = Genre.objects.create(name="Mechas")
        self.user = User.objects.create(
            username="lil",
            email="lilly@torrez.com",
            first_name="lily",
            last_name="torrez",
            password="#alejo77@",
        )
        self.movie = Movie.objects.create(
            title="Takken Tonpa Gurren Lagan",
            release_date=timezone.now(),
            genre=self.genre,
            plot="Falsy movie XD",
        )
        self.rating = Rating.objects.create(
            rating=4,
            comment="Beautiful",
            movie=self.movie,
            user=self.user,
        )

    def test_movie_creation(self):
        self.assertEqual(self.rating.rating, 4)
        self.assertEqual(self.rating.comment, "Beautiful")
        self.assertEqual(self.rating.movie, self.movie)
        self.assertEqual(self.rating.user, self.user)

    def test_200_status_code(self):
        response = self.client.get(reverse("rating:ratings"))
        self.assertEqual(response.status_code, HTTP_STATUS.OK.value)

    def test_movie_str_representation(self):
        self.assertEqual(str(self.rating), f"{self.movie}: {self.rating.rating}")

    def test_delete_movie(self):
        rating_pk = self.rating.pk
        user_pk = self.user.pk
        movie_pk = self.movie.pk
        genre_pk = self.genre.pk
        self.rating.delete()
        self.user.delete()
        self.movie.delete()
        self.genre.delete()

        models = [
            Rating.objects.filter(pk=rating_pk).exists(),
            User.objects.filter(pk=user_pk).exists(),
            Movie.objects.filter(pk=movie_pk).exists(),
            Genre.objects.filter(pk=genre_pk).exists(),
        ]
        self.assertFalse(all(models))
