from django.urls import path

from .views import CreateRating, DeleteRating, RatingById, Ratings, UpdateRating

app_name = "rating"


urlpatterns = [
    path("", Ratings.as_view(), name="ratings"),
    path("create", CreateRating.as_view(), name="create_rating"),
    path("<int:pk>", RatingById.as_view(), name="rating"),
    path("<int:pk>/update", UpdateRating.as_view(), name="update_rating"),
    path("<int:pk>/delete", DeleteRating.as_view(), name="delete_rating"),
]
