from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from rest_framework.permissions import AllowAny, IsAuthenticated

from .models import Rating
from .serializers import RatingSerializer

# Create your views here.


# movies CRUD
class Ratings(ListAPIView):
    queryset = Rating.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RatingSerializer


class RatingById(RetrieveAPIView):
    queryset = Rating.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = RatingSerializer


class CreateRating(CreateAPIView):
    queryset = Rating
    permission_classes = (IsAuthenticated,)
    serializer_class = RatingSerializer


class UpdateRating(UpdateAPIView):
    queryset = Rating.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = RatingSerializer


class DeleteRating(DestroyAPIView):
    queryset = Rating.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = RatingSerializer
