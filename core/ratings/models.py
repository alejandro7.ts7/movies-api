from django.db import models

from core.movies.models import Movie
from core.users.models import User
from core.utils.soft_delete import SoftDeleteModel

# Create your models here.


class Rating(SoftDeleteModel):
    rating = models.IntegerField(default=0)
    comment = models.CharField(max_length=1500)
    movie = models.ForeignKey(to=Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.movie}: {self.rating}"
