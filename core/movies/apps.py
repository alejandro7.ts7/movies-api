from django.apps import AppConfig


class MoviesConfig(AppConfig):
    name = "core.movies"
