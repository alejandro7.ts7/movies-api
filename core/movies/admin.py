from django.contrib import admin

from .models import Genre, Movie

# Register your models here.


@admin.register(Movie, Genre)
class MoviesAdmin(admin.ModelAdmin):
    pass
