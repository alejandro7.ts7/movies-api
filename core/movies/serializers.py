from django.db.models import Avg
from rest_framework import serializers

from core.ratings.models import Rating

from .models import Genre, Movie


class MovieSerializer(serializers.ModelSerializer):
    rating_average = serializers.SerializerMethodField()
    genre_name = serializers.SerializerMethodField()

    class Meta:
        model = Movie
        fields = "__all__"

    def get_rating_average(self, instance):
        return Rating.objects.filter(movie_id=instance.id).aggregate(Avg("rating"))[
            "rating__avg"
        ]

    def get_genre_name(self, instance):
        return instance.genre.name


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = "__all__"
