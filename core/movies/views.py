from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from rest_framework.permissions import AllowAny, IsAuthenticated

from .models import Genre, Movie
from .serializers import GenreSerializer, MovieSerializer

# Create your views here.


# movies CRUD
class Movies(ListAPIView):
    queryset = Movie.objects.all().order_by("release_date")
    permission_classes = (AllowAny,)
    serializer_class = MovieSerializer


class MovieById(RetrieveAPIView):
    queryset = Movie.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = MovieSerializer


class CreateMovie(CreateAPIView):
    queryset = Movie
    permission_classes = (IsAuthenticated,)
    serializer_class = MovieSerializer


class UpdateMovie(UpdateAPIView):
    queryset = Movie.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = MovieSerializer


class DeleteMovie(DestroyAPIView):
    queryset = Movie.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = MovieSerializer


# genre CRUD
class Genres(ListAPIView):
    queryset = Genre.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = GenreSerializer


class GenreById(RetrieveAPIView):
    queryset = Genre.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = GenreSerializer


class CreateGenre(CreateAPIView):
    queryset = Genre
    permission_classes = (IsAuthenticated,)
    serializer_class = GenreSerializer


class UpdateGenre(UpdateAPIView):
    queryset = Genre.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = GenreSerializer


class DeleteGenre(DestroyAPIView):
    queryset = Genre.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = GenreSerializer
