from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from core.utils.http_status import HTTP_STATUS

from .models import Genre, Movie

# Create your tests here.


class GenreModelViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.genre = Genre.objects.create(name="Mechas")

    def test_genre_creation(self):
        self.assertEqual(self.genre.name, "Mechas")

    def test_401_status_code(self):
        response = self.client.get(reverse("movies:genres"))
        self.assertEqual(response.status_code, HTTP_STATUS.UNAUTHORIZE.value)

    def test_genre_str_representation(self):
        self.assertEqual(str(self.genre), "Mechas")

    def test_delete_genre(self):
        pk = self.genre.pk
        self.genre.delete()
        self.assertFalse(Genre.objects.filter(pk=pk).exists())


class MovieModelViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.genre = Genre.objects.create(name="Mechas")
        self.movie = Movie.objects.create(
            title="Takken Tonpa Gurren Lagan",
            release_date=timezone.now(),
            genre=self.genre,
            plot="Falsy movie XD",
        )

    def test_movie_creation(self):
        self.assertEqual(self.movie.title, "Takken Tonpa Gurren Lagan")
        self.assertEqual(self.movie.release_date.date(), timezone.now().date())
        self.assertEqual(self.movie.genre, self.genre)
        self.assertEqual(self.movie.plot, "Falsy movie XD")

    def test_200_status_code(self):
        response = self.client.get(reverse("movies:movies"))
        self.assertEqual(response.status_code, HTTP_STATUS.OK.value)

    def test_movie_str_representation(self):
        self.assertEqual(str(self.movie), "Takken Tonpa Gurren Lagan")

    def test_delete_movie(self):
        pk = self.movie.pk
        self.movie.delete()
        self.assertFalse(Movie.objects.filter(pk=pk).exists())
