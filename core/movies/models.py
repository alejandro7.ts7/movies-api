from django.db import models

from core.utils.soft_delete import SoftDeleteModel

# Create your models here.


class Genre(SoftDeleteModel):
    name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.name


class Movie(SoftDeleteModel):
    title = models.CharField(max_length=55)
    release_date = models.DateTimeField()
    genre = models.ForeignKey(to=Genre, on_delete=models.CASCADE)
    plot = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.title
