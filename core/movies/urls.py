from django.urls import path

from .views import (
    CreateGenre,
    CreateMovie,
    DeleteGenre,
    DeleteMovie,
    GenreById,
    Genres,
    MovieById,
    Movies,
    UpdateGenre,
    UpdateMovie,
)

app_name = "movies"


urlpatterns = [
    path("", Movies.as_view(), name="movies"),
    path("create", CreateMovie.as_view(), name="create_movie"),
    path("<int:pk>", MovieById.as_view(), name="movie"),
    path("<int:pk>/update", UpdateMovie.as_view(), name="update_movie"),
    path("<int:pk>/delete", DeleteMovie.as_view(), name="delete_movie"),
    path("genres/", Genres.as_view(), name="genres"),
    path("genres/create", CreateGenre.as_view(), name="create_genre"),
    path("genres/<int:pk>", GenreById.as_view(), name="genre"),
    path("genres/<int:pk>/update", UpdateGenre.as_view(), name="update_genre"),
    path("genres/<int:pk>/delete", DeleteGenre.as_view(), name="delete_genre"),
]
