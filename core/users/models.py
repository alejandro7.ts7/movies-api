from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


# Create your models here.
class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")

        email = self.normalize_email(email)
        email = email.lower()

        user = self.model(
            email=email,
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password=None):
        user = self.create_user(
            username=username,
            email=email,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractUser, PermissionsMixin):
    """
    default custom user model for schedule project
    """

    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    objects = UserManager()

    def get_absolute_url(self):
        """Get url for user details view"""
        return reverse(
            "users:details",
            kwargs={
                "username": self.username,
                "email": self.email,
            },
        )

    class Meta:
        verbose_name = "Custom User"
