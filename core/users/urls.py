from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from .views import MyTokenObtainPairView, Register, VerifyUser

app_name = "users"

urlpatterns = [
    path("signup/", Register.as_view(), name="register"),
    path("signin/", MyTokenObtainPairView.as_view(), name="login"),
    path("verify/", VerifyUser.as_view(), name="verify"),
    path("refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]
