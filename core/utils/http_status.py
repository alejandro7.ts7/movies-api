from enum import Enum


class HTTP_STATUS(Enum):
    CREATED = 201
    OK = 200
    UNAUTHORIZE = 401
