from rest_framework.generics import CreateAPIView, DestroyAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated

from .models import Watchlist
from .serializers import WatchlistSerializer

# Create your views here.


class Watchlist(ListAPIView):
    queryset = Watchlist.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = WatchlistSerializer

    def get_queryset(self):
        if self.request.user:
            return self.queryset.filter(user=self.request.user)


class AddMovie(CreateAPIView):
    queryset = Watchlist
    permission_classes = (IsAuthenticated,)
    serializer_class = WatchlistSerializer


class RemoveMovie(DestroyAPIView):
    queryset = Watchlist
    permission_classes = (IsAuthenticated,)
    serializer_class = WatchlistSerializer
