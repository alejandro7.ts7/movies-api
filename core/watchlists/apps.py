from django.apps import AppConfig


class WatchlistsConfig(AppConfig):
    name = "core.watchlists"
