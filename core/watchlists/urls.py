from django.urls import path

from .views import AddMovie, RemoveMovie, Watchlist

app_name = "Watchlist"


urlpatterns = [
    path("", Watchlist.as_view(), name="watchlist"),
    path("add", AddMovie.as_view(), name="add_movie"),
    path("<int:pk>/remove", RemoveMovie.as_view(), name="remove_movie"),
]
