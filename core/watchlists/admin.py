from django.contrib import admin

from .models import Watchlist

# Register your models here.


@admin.register(Watchlist)
class WatchlistAdmin(admin.ModelAdmin):
    pass
