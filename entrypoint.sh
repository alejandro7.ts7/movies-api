#!/bin/bash

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z "$SQL_HOST" "$SQL_PORT"; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py flush --no-input

# run migrations
python manage.py migrate

# fixtures

python manage.py loaddata fixtures/user_fixtures.json --app users.user
python manage.py loaddata fixtures/genre_fixtures.json --app movies.genre
python manage.py loaddata fixtures/movies_fixtures.json --app movies.movie
python manage.py loaddata fixtures/ratings_fixture.json --app ratings.rating
python manage.py loaddata fixtures/watchlists_fixtures.json --app watchlists.watchlist

# tests
python manage.py test

# start server
python manage.py runserver 0.0.0.0:8000

exec "$@"
